#ifndef MATRIX_H
#define MATRIX_H

#include <algorithm>
#include <cmath>
#include <ostream>

#include "polynomial.h"

/// Klasa macierzy wielomianw
class PolyMatrix {
private:
	static const unsigned int LIMIT = 8;
	Polynomial Data[LIMIT][LIMIT];
public:
	
	unsigned int N;	///< Liczba kolumn
	unsigned int M;	///< Liczba wierszy

	PolyMatrix();
	PolyMatrix(unsigned int n, unsigned int m);
	PolyMatrix(const PolyMatrix & a);

	/// @return maksymalna liczb kolumn/wierszy
	static unsigned int limit() {
		return LIMIT;
	}

	void insert(unsigned int, unsigned int, const Polynomial &);
	PolyMatrix& operator=(const PolyMatrix  &);
	PolyMatrix  operator+(const PolyMatrix &) const;
	PolyMatrix  operator*(const PolyMatrix &) const;
	PolyMatrix&  operator*=(const Polynomial &);
	PolyMatrix  operator*(const Polynomial &) const;
	PolyMatrix& operator*=(const PolyMatrix &);
	Polynomial& operator()(unsigned int, unsigned int);
	bool operator==(const PolyMatrix &) const;
	bool operator!=(const PolyMatrix &) const;

	friend std::ostream& operator<<(std::ostream&, const PolyMatrix);
};

#endif
