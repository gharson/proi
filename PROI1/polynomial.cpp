#include "polynomial.h"

#include <cctype>
#include <exception>
#include <stdexcept>

//const int Polynomial::N;

/**
*	Konstruktor bezparametrowy - tworzy wielomian zerowy.
*/
Polynomial::Polynomial() {
	for (int i = 0; i<capacity(); i++) Factors[i] = 0;
}

/**
*	Konstruktor tworzący wielomian na podstawie tablicy współczynników.
*	@param T tablica współczynników, w kolejności rosnących potęg (i-ty element odpowiada współczynnikowi przy -tej potędze)
*	@param n liczba elementów tablicy T
*/
Polynomial::Polynomial(double* T, int n) {
	//n = min(capacity(), n);
	if (n > capacity()) throw std::length_error("Given array is too big");
	else {
		for (int i = 0; i<n; i++) Factors[i] = T[i];
		for (int i = n; i<capacity(); i++) Factors[i] = 0;
	}
}

/**
*	Konstruktor wielomianu tożsamościowo równemu stałej.
*	@param a wyraz wolny wielomianu
*/
Polynomial::Polynomial(double a) {
	Factors[0] = a;
	for (int i = 1; i<capacity(); i++) Factors[i] = 0;
}

/**
*	Konstruktor kopiujący.
*	@param a refeerencja na inny wielomian
*/
Polynomial::Polynomial(const Polynomial & a) {
	for (int i = 0; i < capacity(); i++) Factors[i] = a.Factors[i];
}

/**
*	Operator przypisania.
*	@param a referencja na inny wielomian
*/
Polynomial& Polynomial::operator=(const Polynomial & a){
	for (int i = 0; i < capacity(); i++) Factors[i] = a.Factors[i];
	return *this;
}

/**
*	Mnoży wielomian przez stałą.
*	@param a liczba rzeczywista
*	@return referencja na obecny wielomian
*/
Polynomial& Polynomial::operator*=(const double a){
	for (int i = 0; i<capacity(); i++) Factors[i] *= a;
	return *this;
}

/**
*	Dodaje do wielomianu inny wielomian.
*	@param a referencja na inny wielomian
*	@return referencja na obecny wielomian
*/
Polynomial& Polynomial::operator+=(const Polynomial & a){
	for (int i = 0; i < capacity(); i++) Factors[i] += a.Factors[i];
	return *this;
}

/**
*	Mnoży wielomian przez inny wielomian.
*	@param a referencja na inny wielomian
*	@return referencja na obecny wielomian
*/
Polynomial& Polynomial::operator*=(const Polynomial & a){
	Polynomial buffer(*this);
	*this = a*buffer;
	return *this;
}

/**
*	Operator dostępu - zwraca referencję na współczynnik przy podanej potędze.
*	@param k wykładnik wybranej potęgi
*	@return referencja na współczynnik przy k-tej potędze
*/
double& Polynomial::operator[](int k) {
	if (k >= N || k < 0) throw std::out_of_range("Trying to read non-existing element of array");
	return Factors[k];
}

/**
*	Mnoży dwa wielomiany. Współczynniki przy zbyt dużych potęgach zostaną pominięte.
*	@param a referencja na inny wielomian
*	@return nowy wielomian będący iloczynem obecnego wielomianu i a
*/
Polynomial Polynomial::operator*(const Polynomial & a) const{
	Polynomial res;
	for (int i = 0; i < capacity(); i++)
		for (int j = 0; j < capacity() - i; j++)
			res.Factors[i + j] += Factors[i] * a.Factors[j];
	return res;
}

/**
*	Dodaje dwa wielomiany.
*	@param a referencja na inny wielomian
*	@return nowy wielomian będący sumą obecnego wielomianu i a
*/
Polynomial Polynomial::operator+(const Polynomial & a) const{
	Polynomial res((double*)Factors, capacity());
	for (int i = 0; i < capacity(); i++) res.Factors[i] += a.Factors[i];
	return res;
}

/**
*	Mnoży wielomian przez stałą
*	@param a liczba rzeczywista
*	@return nowy wielomian będący iloczynem obecnego wielomianu i a
*/
Polynomial Polynomial::operator*(const double a) const{
	Polynomial res((double*)Factors, capacity());
	for (int i = 0; i<capacity(); i++) res.Factors[i] *= a;
	return res;
}

/**
*	Operator strumieniowy - wypisuje wielomian w intuicyjnej postaci
*	@param os referencja na strumień wyjściowy
*	@param P wielomian przeznaczony do wypisania
*	@return referencja na strumień os
*/
std::ostream& operator<<(std::ostream & os, const Polynomial & P) {
	bool b = false;
	for (int i = P.capacity() - 1; i >= 0; i--) {
		if (P.Factors[i] != 0) {
			if (b && P.Factors[i] > 0) os << " + ";
			if (P.Factors[i] < 0) os << " - ";
			if (fabs(P.Factors[i]) != 1 || i==0) os << fabs(P.Factors[i]);
			if (i > 0) os << "x";
			if (i > 1) os << "^" << i;
			b = true;
		}
	}
	if (!b) os << "0";
	return os;
}

/**
*	Operator porównywania
*	@param a referencja na wielomian do porównania
*	@return true, jeśli oba wielomiany są tożsamościowo równe (mają równe współczynniki)
*/
bool Polynomial::operator==(const Polynomial & a) const {
	for (int i = 0; i < capacity(); i++) if (Factors[i] != a.Factors[i]) return false;
	return true;
}

/**
*	@param a referencja na wielomian do porównania
*	@return false, jeśli oba wielomiany są tożsamościowo równe (mają równe współczynniki)
*/
bool Polynomial::operator!=(const Polynomial & a) const {
	return !((*this) == a);
}
