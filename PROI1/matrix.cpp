#include "matrix.h"

#include <exception>
#include <stdexcept>

/**
*	Konstruktor bezparametrowy.
*	Tworzy macierz 0x0.
*/
PolyMatrix::PolyMatrix() {
	N = M = 0;
}


/**
*	Konstruktor macierzy NxM.
*	@param n liczba kolumn
*	@param m liczba wierszy
*/
PolyMatrix::PolyMatrix(unsigned int n, unsigned int m) {
	if (n > limit() || m > limit()) throw std::length_error("Trying to create too big matrix");
	else {
		M = m;
		N = n;
	}
	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			Data[i][j] = Polynomial(0.0);
}

/**
*	Konstruktor kopiujący.
*	@param a referencja na macierz
*/
PolyMatrix::PolyMatrix(const PolyMatrix & a) {
	N = a.N; M = a.M;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++) {
			Data[i][j] = a.Data[i][j];
		}
	}
}

/**
*	Operator przypisania.
*	@param a referencja na macierz
*/
PolyMatrix& PolyMatrix::operator=(const PolyMatrix & a) {
	N = a.N; M = a.M;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++) {
			Data[i][j] = a.Data[i][j];
		}
	}
	return *this;
}

/**
*	Operator strumieniowy 
*	@param os referencja na strumień wyjściowy
*	@param a macierz przeznaczona do wypisania
*	@return referencja na strumień os
*/
std::ostream& operator<<(std::ostream& os, const PolyMatrix a) {
	for (int i = 0; i < a.N; i++) {
		for (int j = 0; j < a.M; j++) {
			os << a.Data[i][j] << "\t";
		}
		os << std::endl;
	}
	return os;
}

/**
*	Operator dostępu do elementu
*	@param x numer kolumny
*	@param y numer wiersza
*	@return referencja na wskazany element macierzy
*/
Polynomial& PolyMatrix::operator()(unsigned int x, unsigned int y) {
	if (x >= N || y >= M) throw std::out_of_range("Trying to reach non-existing matrix element");
	return Data[x][y];
}

/**
*	Zastępuje wskazany element macierzy
*	@param x numer kolumny
*	@param y numer wiersza
*	@param element element (wielomian) do wstawienia we wskazanej komórce macierzy
*/
void PolyMatrix::insert(unsigned int x, unsigned int y, const Polynomial & element) {
	if (x >= N || y >= M) throw std::out_of_range("Trying to reach non-existing matrix element");
	Data[x][y] = element;
}

/**
*	Operator dodawania. Dodaje do macierzy macierz o takich samych wymiarach.
*	@param a macierz o wymiarach NxM
*	@return macierz będąca sumą obecnej i podanej (a)
*/
PolyMatrix PolyMatrix::operator+(const PolyMatrix & a) const{
	if (a.N != N || a.M != M) throw std::invalid_argument("Added matrices must have the same size");
	PolyMatrix res(N, M);
	for (int i = 0; i < N; i++)
	for (int j = 0; j < M; j++)
		res.Data[i][j] = Data[i][j] + a.Data[i][j];
	return res;
}

/**
*	Operator mnożenia. Mnoży macierze jedynie wtedy, kiedy ich wymiary są poprawne, tj. liczba kolumn obecnej macierzy musi być równy liczbie wierszy macierzy a.
*	@param a macierz o odpowiednich wymiarach
*	@return macierz będąca iloczynem obecnej i podanej (a)
*	@see operator*=(const PolyMatrix&)
*/
PolyMatrix PolyMatrix::operator*(const PolyMatrix & a) const{
	if (a.M != N) 
		throw std::invalid_argument("Invalid matrices sizes to be multiplied");
	PolyMatrix res(a.N, M);
	for (int i = 0; i < res.N; i++)
		for (int j = 0; j < res.M; j++)
			for (int k = 0; k < N; k++)
				res.Data[i][j] += Data[i][k] * a.Data[k][j];
	return res;
}

/**
*	Mnoży macierze jedynie wtedy, kiedy ich wymiary są poprawne, tj. liczba kolumn obecnej macierzy musi być równy liczbie wierszy macierzy a.
*	@param a macierz o odpowiednich wymiarach
*	@return referencja na obecną macierz
*	@see operator*(const PolyMatrix&);
*/
PolyMatrix& PolyMatrix::operator*=(const PolyMatrix & a) {
	(*this) = a * (*this);
	return *this;
}

/**
*	Mnoży macierz przez pojedyńczy element (wielomian). 
*	@param a wielomian
*	@return referencja na obecną macierz
*	@see operator*(const Polynomial&);
*/
PolyMatrix& PolyMatrix::operator*=(const Polynomial & a){
	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			Data[i][j] *= a;
	return *this;
}

/**
*	Mnoży macierz przez pojedyńczy element (wielomian).
*	@param a wielomian
*	@return nowa macierz pomnożona przez wielomian a
*	@see operator*=(const Polynomial&);
*/
PolyMatrix PolyMatrix::operator*(const Polynomial & a) const{
	PolyMatrix res(*this);
	res *= a;
	return res;
}

/**
*	Operator porównywania
*	@param a referencja na macierz do porównania
*	@return true, jeśli obie macierze są równe
*/
bool PolyMatrix::operator==(const PolyMatrix & a) const {
	if (N != a.N || M != a.M) return false;
	for(int i=0; i<N; i++)
		for(int j=0; j<M; j++)
			if (Data[i][j] != a.Data[i][j]) return false;
	return true;
}

/**
*	@param a referencja na macierz do porównania
*	@return false, jeśli obie macierze są równe
*/
bool PolyMatrix::operator!=(const PolyMatrix & a) const {
	return !((*this) == a);
}