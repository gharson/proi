#include "gtest/gtest.h"
#include "matrix.h"

TEST(MatrixBasicTest, DefaultCtrTest)
{
	PolyMatrix M;
	EXPECT_EQ(0, M.N);
	EXPECT_EQ(0, M.M);
}

TEST(MatrixBasicTest, SizeCtrTest)
{
	PolyMatrix M(3,2);
	ASSERT_EQ(3, M.N);
	ASSERT_EQ(2, M.M);
	EXPECT_EQ(Polynomial(0.0), M(1, 1));
}

TEST(MatrixBasicTest, AccessTest)
{
	PolyMatrix M(3, 2);
	M.insert(1, 1, Polynomial(5.0));
	EXPECT_EQ(Polynomial(5.0), M(1, 1));
}

TEST(MatrixCalculationTest, AddTest)
{
	PolyMatrix P(2, 2), Q(2, 2);
	double buff[3] = { 1, 1, 0 };
	P(0, 0) = Polynomial(buff, 3);
	buff[0] = -1; buff[1] = 1; buff[2] = 0;
	P(0, 1) = Polynomial(buff, 3);
	buff[0] = 0; buff[1] = 1; buff[2] = 0;
	P(1, 0) = Polynomial(buff, 3);
	buff[0] = 0; buff[1] = 2; buff[2] = 0;
	P(1, 1) = Polynomial(buff, 3);

	buff[0] = 1; buff[1] = 0; buff[2] = 1;
	Q(0, 0) = Polynomial(buff, 3);
	buff[0] = 1; buff[1] = 1; buff[2] = 0;
	Q(0, 1) = Polynomial(buff, 3);
	buff[0] = 0; buff[1] = 0; buff[2] = 1;
	Q(1, 0) = Polynomial(buff, 3);
	buff[0] = 0; buff[1] = -1; buff[2] = 0;
	Q(1, 1) = Polynomial(buff, 3);
	
	PolyMatrix W = P + Q;

	buff[0] = 2; buff[1] = 1; buff[2] = 1;
	EXPECT_EQ(Polynomial(buff, 3), W(0, 0));
	buff[0] = 0; buff[1] = 2; buff[2] = 0;
	EXPECT_EQ(Polynomial(buff, 3), W(0, 1));
	buff[0] = 0; buff[1] = 1; buff[2] = 1;
	EXPECT_EQ(Polynomial(buff, 3), W(1, 0));
	buff[0] = 0; buff[1] = 1; buff[2] = 0;
	EXPECT_EQ(Polynomial(buff, 3), W(1, 1));
}

TEST(MatrixCalculationTest, MultiplyTest)
{
	PolyMatrix P(2, 2), Q(2, 2);
	double buff[4] = { 1, 1, 0, 0 };
	P(0, 0) = Polynomial(buff, 4);
	buff[0] = -1; buff[1] = 1; buff[2] = 0;
	P(0, 1) = Polynomial(buff, 4);
	buff[0] = 0; buff[1] = 1; buff[2] = 0;
	P(1, 0) = Polynomial(buff, 4);
	buff[0] = 0; buff[1] = 2; buff[2] = 0;
	P(1, 1) = Polynomial(buff, 4);

	buff[0] = 1; buff[1] = 0; buff[2] = 1;
	Q(0, 0) = Polynomial(buff, 4);
	buff[0] = 1; buff[1] = 1; buff[2] = 0;
	Q(0, 1) = Polynomial(buff, 4);
	buff[0] = 0; buff[1] = 0; buff[2] = 1;
	Q(1, 0) = Polynomial(buff, 4);
	buff[0] = 0; buff[1] = -1; buff[2] = 0;
	Q(1, 1) = Polynomial(buff, 4);

	PolyMatrix W = P * Q;

	buff[0] = 1; buff[1] = 1; buff[2] = 0; buff[3] = 2;
	EXPECT_EQ(Polynomial(buff, 4), W(0, 0));
	buff[0] = 1; buff[1] = 3; buff[2] = 0; buff[3] = 0;
	EXPECT_EQ(Polynomial(buff, 4), W(0, 1));
	buff[0] = 0; buff[1] = 1; buff[2] = 0; buff[3] = 3;
	EXPECT_EQ(Polynomial(buff, 4), W(1, 0));
	buff[0] = 0; buff[1] = 1; buff[2] = -1; buff[3] = 0;
	EXPECT_EQ(Polynomial(buff, 4), W(1, 1));
}

TEST(MatrixCalculationTest, MultiplyByConstTest) 
{
	PolyMatrix P(2, 2);
	double buff[4] = { 1, 1, 0, 0 };
	P(0, 0) = Polynomial(buff, 4);
	buff[0] = -1; buff[1] = 1; buff[2] = 0;
	P(0, 1) = Polynomial(buff, 4);
	buff[0] = 0; buff[1] = 1; buff[2] = 0;
	P(1, 0) = Polynomial(buff, 4);
	buff[0] = 0; buff[1] = 2; buff[2] = 0;
	P(1, 1) = Polynomial(buff, 4);
	
	buff[0] = 1; buff[1] = 1; buff[2] = 0; buff[3] = 0;
	Polynomial Q(buff, 4);

	PolyMatrix W = P*Q;

	buff[0] = 1; buff[1] = 2; buff[2] = 1;
	EXPECT_EQ(Polynomial(buff, 4), W(0, 0));
	buff[0] = -1; buff[1] = 0; buff[2] = 1;
	EXPECT_EQ(Polynomial(buff, 4), W(0, 1));
	buff[0] = 0; buff[1] = 1; buff[2] = 1;
	EXPECT_EQ(Polynomial(buff, 4), W(1, 0));
	buff[0] = 0; buff[1] = 2; buff[2] = 2;
	EXPECT_EQ(Polynomial(buff, 4), W(1, 1));
}