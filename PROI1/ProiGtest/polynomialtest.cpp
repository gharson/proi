#include "gtest/gtest.h"
#include "polynomial.h"

TEST(PolynomialBasicTest, ConstCtrTest)
{
	Polynomial P(2);
	ASSERT_EQ(2, P[0]);
}

TEST(PolynomialBasicTest, DefaultCtrTest)
{
	Polynomial P;
	for (int i = 0; i < P.capacity(); i++) EXPECT_EQ(0, P[i]);
}

TEST(PolynomialBasicTest, FullCtrTest)
{
	double buffer[] = { 1.0, 2.0, 3.0, 4.5 };
	Polynomial P(buffer, 4);
	EXPECT_EQ(1.0, P[0]);
	EXPECT_EQ(2.0, P[1]);
	EXPECT_EQ(3.0, P[2]);
	EXPECT_EQ(4.5, P[3]);
	EXPECT_EQ(0, P[4]);
}

TEST(PolynomialBasicTest, AccessTest)
{
	double buffer[] = { 1.0, 2.0, 3.0, 4.5 };
	Polynomial P(buffer, 4);
	P[6] = 13.37;
	EXPECT_EQ(13.37, P[6]);
}

TEST(PolynomialCalculationTest, AddTest)
{
	double buffer1[] = { 1.0, 2.0, 3.0, 4.5 };
	double buffer2[] = { 2.0, 0.0, 2.0, -4.5 };
	Polynomial P(buffer1, 4), Q(buffer2, 4);
	Polynomial W;
	W = P + Q;
	for (int i = 0; i < 4; i++) EXPECT_EQ(buffer1[i] + buffer2[i], W[i]) << "Wrong " << i << "-th factor of result";
}

TEST(PolynomialCalculationTest, AssignAddTest)
{
	double buffer1[] = { 1.0, 2.0, 3.0, 4.5 };
	double buffer2[] = { 2.0, 0.0, 2.0, -4.5 };
	Polynomial P(buffer1, 4), Q(buffer2, 4);
	P += Q;
	for (int i = 0; i < 4; i++) EXPECT_EQ(buffer1[i] + buffer2[i], P[i]) << "Wrong " << i << "-th factor of result";
}

TEST(PolynomialCalculationTest, MultiplyTest)
{
	double buffer1[] = { 7.0, 3.5, 2.0, 0.0, 1.0 };
	double buffer2[] = { 1.5, 2.0, 0.0, 3.0 };
	double expected[] = { 10.5, 19.25, 10.0, 25.0, 12.0, 8.0, 0.0, 3.0 };
	Polynomial P(buffer1, 5), Q(buffer2, 4), W;
	W = P*Q;
	for (int i = 0; i < 8; i++) EXPECT_EQ(expected[i], W[i]) << "Wrong " << i << "-th factor of result";
}

TEST(PolynomialCalculationTest, MultiplyAssignTest)
{
	double buffer1[] = { 7.0, 3.5, 2.0, 0.0, 1.0 };
	double buffer2[] = { 1.5, 2.0, 0.0, 3.0 };
	double expected[] = { 10.5, 19.25, 10.0, 25.0, 12.0, 8.0, 0.0, 3.0 };
	Polynomial P(buffer1, 5), Q(buffer2, 4);
	P *= Q;
	for (int i = 0; i < 8; i++) EXPECT_EQ(expected[i], P[i]) << "Wrong " << i << "-th factor of result";
}

TEST(PolynomialCalculationTest, ConstMultiplyTest)
{
	double buffer1[] = { 7.0, 3.5, 2.0, 0.0, 1.0 };
	const double C = 0.5;
	double expected[] = { 3.5, 1.75, 1.0, 0.0, 0.5 };
	Polynomial P(buffer1, 5), W;
	W = P*C;
	for (int i = 0; i < 5; i++) EXPECT_EQ(expected[i], W[i]) << "Wrong " << i << "-th factor of result";
}

TEST(PolynomialCalculationTest, ConstMultiplyAssignTest)
{
	double buffer1[] = { 7.0, 3.5, 2.0, 0.0, 1.0 };
	const double C = 0.5;
	double expected[] = { 3.5, 1.75, 1.0, 0.0, 0.5 };
	Polynomial P(buffer1, 5);
	P *= C;
	for (int i = 0; i < 5; i++) EXPECT_EQ(expected[i], P[i]) << "Wrong " << i << "-th factor of result";
}

TEST(PolynomialAditionalTest, OutputTest)
{
	double buffer[] = { 1.0, 2.0, 3.0, -4.5 };
	Polynomial P(buffer, 4);
	std::string expected, actual;
	std::stringstream ss;
	ss << P;
	actual = ss.str();
	expected = " - 4.5x^3 + 3x^2 + 2x + 1";
	EXPECT_EQ(expected, actual);
}
