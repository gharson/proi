#ifndef POLYNOMIAL_H
#define POLYNOMIAL_H

#include <vector>
#include <algorithm>
#include <cmath>
#include <cstdarg>
#include <ostream>
#include <string>

/// Klasa reprezentująca wielomian o współczynnikach rzeczywistych
class Polynomial {
private:
	static const int N = 32;
	double Factors[N];
public:
	Polynomial();
	Polynomial(double*, int);
	Polynomial(double);
	Polynomial(const Polynomial &);
	
	/** @return maksymalna liczba wspolczynnikow*/
	static int capacity() {
		return N;
	}

	Polynomial& operator=(const Polynomial &);
	Polynomial operator*(const double) const;
	Polynomial& operator*=(const double);
	Polynomial operator+(const Polynomial&) const;
	Polynomial& operator+=(const Polynomial&);
	Polynomial operator*(const Polynomial&) const;
	Polynomial& operator*=(const Polynomial&);
	bool operator==(const Polynomial&) const;
	bool operator!=(const Polynomial&) const;
	double& operator[](int);

	friend std::ostream& operator<<(std::ostream&, const Polynomial&);

};

#endif
