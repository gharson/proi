var searchData=
[
  ['operator_28_29',['operator()',['../class_poly_matrix.html#a94037f7274db994132dee5189098ae46',1,'PolyMatrix']]],
  ['operator_2a',['operator*',['../class_poly_matrix.html#acdf67cd08b7ff67f5d4f73ffc00bdbfd',1,'PolyMatrix::operator*(const PolyMatrix &amp;) const '],['../class_poly_matrix.html#a0a2e735dab9bd5ed4da01b54173c3d50',1,'PolyMatrix::operator*(const Polynomial &amp;) const '],['../class_polynomial.html#a57a726d0fd4397fdea445849f6578f67',1,'Polynomial::operator*(const double) const '],['../class_polynomial.html#a33054c6b64e7150e793e0e4f190b4263',1,'Polynomial::operator*(const Polynomial &amp;) const ']]],
  ['operator_2a_3d',['operator*=',['../class_poly_matrix.html#a121546145567e13314814004482148b5',1,'PolyMatrix::operator*=(const Polynomial &amp;)'],['../class_poly_matrix.html#aee2179f8994bd638f0ba759b68bb0978',1,'PolyMatrix::operator*=(const PolyMatrix &amp;)'],['../class_polynomial.html#abb5abcab61d941da5d2558cc2cff7420',1,'Polynomial::operator*=(const double)'],['../class_polynomial.html#a54e2436fb2cd108a63b534c9afa1642e',1,'Polynomial::operator*=(const Polynomial &amp;)']]],
  ['operator_2b',['operator+',['../class_poly_matrix.html#a8a1c09c031b017eb9f0b46b0b4ce18a5',1,'PolyMatrix::operator+()'],['../class_polynomial.html#afa31637ba66971695ecf1abc7cb1aa4f',1,'Polynomial::operator+()']]],
  ['operator_2b_3d',['operator+=',['../class_polynomial.html#a8894ca09840d15cc8c70591f713de59c',1,'Polynomial']]],
  ['operator_3d',['operator=',['../class_poly_matrix.html#a66ccb6b6daf6d5e5e0f40c81a0f9ee12',1,'PolyMatrix::operator=()'],['../class_polynomial.html#af834260c70cc1aae5a81dca0d67009a3',1,'Polynomial::operator=()']]],
  ['operator_5b_5d',['operator[]',['../class_polynomial.html#a48746d31f1526b7c25b6c12cd6b3d932',1,'Polynomial']]]
];
