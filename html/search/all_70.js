var searchData=
[
  ['polymatrix',['PolyMatrix',['../class_poly_matrix.html',1,'PolyMatrix'],['../class_poly_matrix.html#aafd3f4c91c3aafff318c6440e233c27a',1,'PolyMatrix::PolyMatrix()'],['../class_poly_matrix.html#a357f69cc86eccaa1a7951f6b930fc9fa',1,'PolyMatrix::PolyMatrix(unsigned int n, unsigned int m)'],['../class_poly_matrix.html#a300afe0d8ff6dbfac258814100393df1',1,'PolyMatrix::PolyMatrix(const PolyMatrix &amp;a)']]],
  ['polynomial',['Polynomial',['../class_polynomial.html',1,'Polynomial'],['../class_polynomial.html#a961dec5c0727f03e5273f74a345dfbb6',1,'Polynomial::Polynomial()'],['../class_polynomial.html#a4b2f23c0326a96799f693338a551584a',1,'Polynomial::Polynomial(double *, int)'],['../class_polynomial.html#a29d68fe61dc377e9dbdc1b2a18645cb1',1,'Polynomial::Polynomial(double)'],['../class_polynomial.html#a1e8cb8f8e5fbd2caead2b4f784b766ab',1,'Polynomial::Polynomial(const Polynomial &amp;)']]]
];
