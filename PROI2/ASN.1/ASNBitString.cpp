#include "ASNBitString.h"

/**
@brief Konstruktor bezparametrowy
*/
ASNBitString::ASNBitString()
{
	length = 0;
	identifier = ASNTypesIdentifier::BITSTRING;
}

/**
@brief Konstruktor przyjmujący wartość jako argument
*/
ASNBitString::ASNBitString(string s) : value(s)
{
	length = s.length();
	identifier = ASNTypesIdentifier::BITSTRING;
}

ASNBitString::~ASNBitString()
{
}

/**
@brief Zapisuje rekord jako ciąg bajtów
@details Dane zapisywane są do strumienia w postaci ciągu bajtów zgodnego z kodowaniem BER (Type-Length-Value).
Typ i długość zajmują pierwsze dwa bajty, po nich następują dane.
*/
ostream& ASNBitString::writeToStream(ostream& os) {
	os.put(identifier);
	os.put(length);
	for (int i = 0; i < value.length(); i++) {
		os.put(value[i]);
	}
	return os;
}

/**
@brief Wczytuje dane ze strumienia
@details Na wejściu powinny pojawić się dane w kodowaniu BER (Type-Length-Value) bez pierwszego bajtu identyfikacji.
Długość kodowana jest jednym bajtem.
*/
istream& ASNBitString::readValueFromStream(istream& is) {
	char c;
	value.clear();
	is.get(c);
	length = c;
	for (int i = 0; i < length; i++) {
		is.get(c);
		value.push_back(c);
	}
	return is;
}

/**
@brief zwraca wartość
@return wartość rekordu
*/
string ASNBitString::getValue() {
	return value;
}

/**
@brief Pozwala ustawić nową wartość
*/
void ASNBitString::setValue(string s) {
	value = s;
	length = value.length();
}
