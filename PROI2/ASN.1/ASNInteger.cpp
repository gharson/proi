#include "ASNInteger.h"
/**
@brief Konstruktor bezparametrowy
*/
ASNInteger::ASNInteger()
{
	length = sizeof(int);
	identifier = ASNTypesIdentifier::INTEGER;
}

ASNInteger::ASNInteger(int v) : value(v)
{
	length = sizeof(int);
	identifier = ASNTypesIdentifier::INTEGER;
}

ASNInteger::~ASNInteger()
{
}

ASNInteger& ASNInteger::operator=(const int v) {
	value = v;
	return *this;
}

/**
@brief Zapisuje rekord jako ciąg bajtów
@details Dane zapisywane są do strumienia w postaci ciągu bajtów zgodnego z kodowaniem BER (Type-Length-Value).
Typ i długość zajmują pierwsze dwa bajty, po nich następują dane.
*/
ostream& ASNInteger::writeToStream(ostream& os) {
	os.put(identifier);
	os.put(length);
	for (int i = 0; i < length; i++)
		os.put(0xFFFF & (value >> (4*i)));
	return os;
}

/**
@brief Wczytuje dane ze strumienia
@details Na wejściu powinny pojawić się dane w kodowaniu BER (Type-Length-Value) bez pierwszego bajtu identyfikacji.
Długość kodowana jest jednym bajtem.
*/
istream& ASNInteger::readValueFromStream(istream& is) {
	char c;
	value = 0;
	is.get(c);
	if (c != length) throw exception("Invalid length byte");
	for (int i = 0; i < length; i++) {
		is.get(c);
		value |= ((int)c) << (4 * i);
	}
	return is;
}

/**
@return wartość rekordu
*/
int ASNInteger::getValue() {
	return value;
}

/**
@brief Pozwala ustawić nową wartość
*/
void ASNInteger::setValue(int v) {
	value = v;
}