#pragma once
#include "ASNObject.h"
#include "ASNEnumType.h"
#include "ASNTypes.h"
#include <map>
#include <string>

using namespace std;

/**
@brief Reprezentuje obiekt typu wyliczeniowego ASN.1
@details Współdziała z obiektami ASNEnumType, które reprezentują typy wyliczeniowe zdefiniowane przez użytkownika.
*/
class ASNEnumerated :
	public ASNObject
{
private:
	ASNEnumType* type;
	int value;
public:

	ASNEnumerated();
	ASNEnumerated(ASNEnumType*);
	~ASNEnumerated();

	ASNEnumerated& operator=(int);
	ASNEnumerated& operator=(string);

	ostream& writeToStream(ostream&);
	istream& readValueFromStream(istream&);

	string getLabel();
	int getValue();
	void setValueByLabel(string);
	void setValueByKey(int);

	void setType(ASNEnumType*);
};

