﻿#pragma once

#include <istream>
#include <ostream>
#include <exception>

using namespace std;

/**
@brief Klasa bazowa wszystkich obiektów ASN
@details Dostarcza podstawowego interfejsu do serializacji/deserializacji danych.
*/
class ASNObject
{
protected:
	char identifier;
	char length;
public:
	ASNObject();
	~ASNObject();

	///Zapisuje do strumienia całość danych
	virtual ostream& writeToStream(ostream&) = 0;
	///Odczytuje ze strumienia dane, począwszy od długości (oktet typu odczytywany jest wcześniej)
	virtual istream& readValueFromStream(istream&) = 0;
	
	char getLength();
	char getIdentifier();
}; 