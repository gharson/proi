#pragma once

#include <istream>
#include <ostream>

using namespace std;

class IDisplayable
{
public:
	IDisplayable();
	~IDisplayable();

	virtual void Display() = 0;
};