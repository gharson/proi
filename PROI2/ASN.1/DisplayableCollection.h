#pragma once
#include "IDisplayable.h"
#include "Collection.h"

#include <string>
#include <iostream>
#include <iomanip>
using namespace std;

/**
@brief Pozwala na wyświetlenie kolekcji obiektów na ekranie
*/
class DisplayableCollection :
	public IDisplayable
{
private:
	Collection<pair<wstring, IDisplayable*> > complexElements;
	Collection<pair<wstring, wstring> > simpleElements;
	string label;

public:
	DisplayableCollection();
	DisplayableCollection(string);
	~DisplayableCollection();

	void addElement(wstring, IDisplayable*);
	void addElement(wstring, wstring);
	void Display();
	void clear();
};