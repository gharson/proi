#include "ASNSequence.h"

/**
@brief Konstruktor bezparametrowy
*/
ASNSequence::ASNSequence()
{
	identifier = ASNTypesIdentifier::SEQUENCE;
	length = 0;
}

ASNSequence::~ASNSequence()
{
}

/**
@brief Zapisuje rekord jako ciąg bajtów
@details Dane zapisywane są do strumienia w postaci ciągu bajtów zgodnego z kodowaniem BER (Type-Length-Value).
Typ i długość zajmują pierwsze dwa bajty, po nich następują dane.
*/
ostream& ASNSequence::writeToStream(ostream& os) {
	os.put(ASNTypesIdentifier::SEQUENCE);
	os.put((char)data.size());
	for (int i = 0; i < data.size(); i++)
		data[i]->writeToStream(os);
	return os;
}

/**
@brief Wczytuje dane ze strumienia
@details Na wejściu powinny pojawić się dane w kodowaniu BER (Type-Length-Value) bez pierwszego bajtu identyfikacji.
Długość kodowana jest jednym bajtem i w tym wypadku oznacza liczbę elementów sekwencji.
*/
istream& ASNSequence::readValueFromStream(istream& is) {
	char c;
	is.get(c);
	for (int i = 0; i < c; i++) {
		ASNObject* a = ASNObjectsFactory::createObjectFromStream(is);
		addField(a);
	}
	return is;
}

/**
@brief Dodaje obiekt do sekwencji
*/
void ASNSequence::addField(ASNObject* a) {
	length ++;
	data.push_back(a);
}