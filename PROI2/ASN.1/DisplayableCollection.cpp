#include "DisplayableCollection.h"


DisplayableCollection::DisplayableCollection()
{
}

DisplayableCollection::DisplayableCollection(string s) : label(s)
{
}

DisplayableCollection::~DisplayableCollection()
{
	//clear();
}

/**
@brief Dodaje złożony element do kolekcji
*/
void DisplayableCollection::addElement(wstring s, IDisplayable* c) {
	complexElements.add(make_pair(s, c));
}

/**
@brief Dodaje prosty element (string) do kolekcji
*/
void DisplayableCollection::addElement(wstring s, wstring c) {
	simpleElements.add(make_pair(s, c));
}

/**
@brief Czyści kolekcję
*/
void DisplayableCollection::clear() {
	while(simpleElements.size() > 0) {
		simpleElements.remove(simpleElements.begin());
	}
	while (complexElements.size() > 0) {
		complexElements.remove(complexElements.begin());
	}
}

/**
@brief Wyświetla całą kolekcję na ekranie
*/
void DisplayableCollection::Display() {
	for (auto it = simpleElements.begin(); it != simpleElements.end(); it++) {
		wcout << it->first << " = " << it->second << endl;
	}
	for (auto it = complexElements.begin(); it != complexElements.end(); it++) {
		wcout << it->first << " = {" << endl;
		it->second->Display();
		wcout << "}" << endl;
	}
}