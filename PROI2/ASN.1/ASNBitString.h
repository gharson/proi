#pragma once
#include "ASNObject.h"
#include "ASNTypes.h"
#include <string>

using namespace std;

/**
@brief Ciąg binarny
*/
class ASNBitString :
	public ASNObject
{
private:
	string value;
public:
	ASNBitString();
	ASNBitString(string);
	~ASNBitString();

	ostream& writeToStream(ostream&);
	istream& readValueFromStream(istream&);

	string getValue();
	void setValue(string);
};

