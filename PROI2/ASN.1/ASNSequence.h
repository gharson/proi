#pragma once
#include "ASNObject.h"
#include "ASNTypes.h"
#include <map>
#include <string>
#include <vector>

using namespace std;

/**
@brief Kolekcja obiektów ASN.1 dowolnego typu
*/
class ASNSequence :
	public ASNObject
{
public:
	vector<ASNObject*> data;
	ASNSequence();
	~ASNSequence();

	ostream& writeToStream(ostream&);
	istream& readValueFromStream(istream&);

	void addField(ASNObject*);
};