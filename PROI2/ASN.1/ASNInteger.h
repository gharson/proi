#pragma once
#include "ASNObject.h"
#include "ASNTypes.h"
#include <string>
#include <map>

/**
@brief Liczba całkowita
*/
class ASNInteger :
	public ASNObject
{
private:
	int value;
public:
	ASNInteger();
	ASNInteger(int);
	~ASNInteger();

	ASNInteger& operator=(const int);

	ostream& writeToStream(ostream&);
	istream& readValueFromStream(istream&);

	int getValue();
	void setValue(int);
};