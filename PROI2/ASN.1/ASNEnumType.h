#pragma once
#include "ASNUserType.h"
#include <map>
#include <string>
#include <exception>
using namespace std;

/**
@brief typ enum definiowany przez użytkownika
*/
class ASNEnumType :
	public ASNUserType
{
private:
	map<string, int> keys;
	char identifier;
	ASNEnumType();
public:
	ASNEnumType(int);
	~ASNEnumType();

	int getKey(string);
	string getLabelByKey(int);
	void addKey(string, int);

	bool keyExists(int);
	bool labelExists(string);
};