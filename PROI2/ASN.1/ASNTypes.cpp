#include "ASNTypes.h"

ASNObject* ASNObjectsFactory::createObjectFromStream(istream& is) {
	char c;
	is.get(c);
	ASNObject* res = NULL;
	if (c == ASNTypesIdentifier::INTEGER) res = new ASNInteger();
	else if (c == ASNTypesIdentifier::BITSTRING) res = new ASNBitString();
	else if (c == ASNTypesIdentifier::UTF8STRING) res = new ASNUTF8String();
	else if (c == ASNTypesIdentifier::ENUMERATED) res = new ASNEnumerated();
	else if (c == ASNTypesIdentifier::SEQUENCE) res = new ASNSequence();
	if (res) res->readValueFromStream(is);
	if (!res) throw exception("Unknown type identifier");
	return res;
}