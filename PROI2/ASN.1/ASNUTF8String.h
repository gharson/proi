#pragma once
#include "ASNObject.h"
#include "ASNTypes.h"
#include <string>

using namespace std;

wstring WstrFromStr(string);

/**
@brief Reprezentuje ciąg znaków
*/
class ASNUTF8String :
	public ASNObject
{
private:
	wstring value;
public:
	ASNUTF8String();
	ASNUTF8String(wstring);
	~ASNUTF8String();

	ASNUTF8String& operator=(wstring);

	ostream& writeToStream(ostream&);
	istream& readValueFromStream(istream&);

	wstring getValue();
	void setValue(wstring);
};
