#include "ASNEnumerated.h"

/**
@brief Konstruktor bezparametrowy
*/
ASNEnumerated::ASNEnumerated()
{
	identifier = ASNTypesIdentifier::ENUMERATED;
}

/**
@brief Konstruktor przyjmujący specyfikację typu jako argument
*/
ASNEnumerated::ASNEnumerated(ASNEnumType* t) : type(t)
{
	identifier = ASNTypesIdentifier::ENUMERATED;
	length = 1;
}

ASNEnumerated::~ASNEnumerated()
{
}

ASNEnumerated& ASNEnumerated::operator=(int a) {
	setValueByKey(a);
	return *this;
}

ASNEnumerated& ASNEnumerated::operator=(string a) {
	setValueByLabel(a);
	return *this;
}

/**
@brief Zapisuje rekord jako ciąg bajtów
@details Dane zapisywane są do strumienia w postaci ciągu bajtów zgodnego z kodowaniem BER (Type-Length-Value).
Typ i długość zajmują pierwsze dwa bajty, po nich następują dane.
*/
ostream& ASNEnumerated::writeToStream(ostream& os) {
	os.put(identifier);
	os.put(length);
	for (int i = 0; i < length; i++)
		os.put(0xFFFF & (value >> (4 * i)));
	return os;
}

/**
@brief Wczytuje dane ze strumienia
@details Na wejściu powinny pojawić się dane w kodowaniu BER (Type-Length-Value) bez pierwszego bajtu identyfikacji.
Długość kodowana jest jednym bajtem.
*/
istream& ASNEnumerated::readValueFromStream(istream& is) {
	char c;
	value = 0;
	is.get(c);
	if (c != length) throw exception("Invalid length byte");
	for (int i = 0; i < length; i++) {
		is.get(c);
		value |= ((int)c) << (4 * i);
	}
	return is;
}

string ASNEnumerated::getLabel() {
	return type->getLabelByKey(value);
}

int ASNEnumerated::getValue() {
	return value;
}

void ASNEnumerated::setValueByLabel(string s) {
	if (!type->labelExists(s)) throw exception((s + ": Key does not exist").c_str());
	value = type->getKey(s);
}

void ASNEnumerated::setValueByKey(int v) {
	if (!type->keyExists(v)) throw exception((to_string(v) + ": Key does not exist").c_str());
	value = v;
}

/**
@brief Ustala typ
*/
void ASNEnumerated::setType(ASNEnumType* t) {
	type = t;
}