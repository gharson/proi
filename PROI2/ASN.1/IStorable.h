#pragma once

#include <string>
#include <istream>
#include <ostream>

using namespace std;

class IStorable
{
public:
	IStorable();
	~IStorable();

	virtual void Save(string filename) = 0;
	virtual void Load(string filename) = 0;
};

