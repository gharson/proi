#pragma once

#include <exception>

/**
@brief Klasa bazowa wszystkich typów definiowanych przez użytkownika (np Enum)
*/
class ASNUserType
{
private:
	/// bajt identyfikacji zdefiniowany przez użytkownika - nie może pokrywać się z wartościami zdefiniowanymi w ASNTypesIdentifier
	char identifier;
public:
	ASNUserType();
	~ASNUserType();
	char getIdentifier();
};