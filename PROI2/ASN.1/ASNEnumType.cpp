#include "ASNEnumType.h"
/**
@brief Konstruktor bezparametrowy
*/
ASNEnumType::ASNEnumType()
{
}

ASNEnumType::ASNEnumType(int v)
{
	identifier = v;
}

ASNEnumType::~ASNEnumType()
{
}

int ASNEnumType::getKey(string s) {
	if (keys.find(s) == keys.end()) throw exception("Key doesn't exist");
	return keys[s];
}

void ASNEnumType::addKey(string s, int v) {
	if (keys.find(s) != keys.end()) throw exception("Key already exists");
	keys[s] = v;
}

string ASNEnumType::getLabelByKey(int v) {
	for (auto it = keys.begin(); it != keys.end(); ++it)
		if (it->second == v)
			return it->first;
	throw exception("Key not found");
}

bool ASNEnumType::keyExists(int v) {
	for (auto it = keys.begin(); it != keys.end(); ++it)
		if (it->second == v)
			return true;
	return false;
}

bool ASNEnumType::labelExists(string s) {
	return keys.find(s) != keys.end();
}