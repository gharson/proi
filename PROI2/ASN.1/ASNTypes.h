#pragma once

#include "ASNObject.h"
#include "ASNInteger.h"
#include "ASNBitString.h"
#include "ASNUTF8String.h"
#include "ASNSequence.h"
#include "ASNEnumerated.h"

/** @file*/
#include <map>
#include <istream>
using namespace std;

/**
@brief Definiuje identyfikatory typów wbudowanych
@details W tej sekcji zdefiniowane są typy podstawowe. Ich identyfikatory zawierają się w przedziale 0-31.
Reszta identyfikatorów zarezerwowana jest dla typów zdefiniowanych przez użytkownika.
Nazwa typu  | Identyfikator typu | Opis
------------|:------------------:|-----
INTEGER		| 2					 |Liczba całkowita (ASNInteger)
BITSTRING	| 3					 |Binarny ciąg znaków (ASNBitString)
UTF8STRING	| 4					 |Ciąg znaków utf8 (ASNUTF8String)
ENUMERATED	| 10				 |Typ wyliczeniowy (ASNEnumerated)
SEQUENCE	| 16				 |Kolekcja zmiennych różnych typów (ASNSequence)
*/
enum ASNTypesIdentifier {
	INTEGER = 2,
	BITSTRING = 3,
	UTF8STRING = 4,
	ENUMERATED = 10,
	SEQUENCE = 16
};

class ASNObjectsFactory
{
private:
	ASNObjectsFactory();
public:
	static ASNObject* createObjectFromStream(istream&);
};