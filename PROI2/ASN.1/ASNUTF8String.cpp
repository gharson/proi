#include "ASNUTF8String.h"

/**
@brief konwertuje string na wstring
*/
wstring WstrFromStr(string s) {
	wstring res;
	for (int i = 0; i < s.size(); i++) res += s[i];
	return res;
}

/**
@brief Konstruktor bezparametrowy
*/
ASNUTF8String::ASNUTF8String()
{
	length = 0;
	identifier = ASNTypesIdentifier::UTF8STRING;
}

/**
@brief Konstruktor przyjmujący wartość jako argument
*/
ASNUTF8String::ASNUTF8String(wstring s) : value(s)
{
	length = s.length() * sizeof(wchar_t);
	identifier = ASNTypesIdentifier::UTF8STRING;
}

ASNUTF8String::~ASNUTF8String()
{
}

ASNUTF8String& ASNUTF8String::operator=(wstring s) {
	value = s;
	length = s.length() * sizeof(wchar_t);
	return *this;
}

/**
@brief Zapisuje rekord jako ciąg bajtów
@details Dane zapisywane są do strumienia w postaci ciągu bajtów zgodnego z kodowaniem BER (Type-Length-Value).
Typ i długość zajmują pierwsze dwa bajty, po nich następują dane.
*/
ostream& ASNUTF8String::writeToStream(ostream& os) {
	os.put(identifier);
	os.put(length);
	for (int i = 0; i < value.length(); i++) {
		for(int j = 0; j < sizeof(wchar_t); j++)
			os.put(0xFFFF & (value[i] >> (4 * j)));
	}
	return os;
}

/**
@brief Wczytuje dane ze strumienia
@details Na wejściu powinny pojawić się dane w kodowaniu BER (Type-Length-Value) bez pierwszego bajtu identyfikacji.
Długość kodowana jest jednym bajtem.
*/
istream& ASNUTF8String::readValueFromStream(istream& is) {
	char c;
	value.clear();
	is.get(c);
	length = c;
	for (int i = 0; i < length/sizeof(wchar_t); i++) {
		wchar_t res = 0;
		for (int j = 0; j < sizeof(wchar_t); j++) {
			is.get(c);
			res |= ((wchar_t)c) << (4 * j);
		}
		value.push_back(res);
	}
	return is;
}

/**
@return wartość rekordu
*/
wstring ASNUTF8String::getValue() {
	return value;
}

/**
@brief Pozwala ustawić nową wartość
*/
void ASNUTF8String::setValue(wstring s) {
	value = s;
	length = sizeof(wchar_t) * value.length();
}