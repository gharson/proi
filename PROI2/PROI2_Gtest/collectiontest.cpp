#include "gtest/gtest.h"
#include "Collection.h"

TEST(CollectionBasicTest, IntCtrTest)
{
	Collection<int> Q;
}

TEST(CollectionBasicTest, IntAddTest)
{
	Collection<int> Q;
	for (int i = 0; i < 7; i++)
		Q.add(i);
	EXPECT_EQ(7, Q.size());
}

struct Record {
	int a;
	double b;
	Record(int x, int y) : a(x), b(y) {}
};

class RecordDispatcher {
public:
	int operator()(Record& r) {
		return r.a;
	}
};

class RecordDoubleDispatcher {
public:
	double operator()(Record& r) {
		return r.b;
	}
};

TEST(CollectionStructuralTest, AddTest)
{
	Collection<Record> Q;
	for (int i = 0; i < 1000; i++)
		Q.add(Record(i, 0.0));
	EXPECT_EQ(1000, Q.size());
}

TEST(CollectionStructuralTest, RemoveTest)
{
	Collection<Record> Q;
	for (int i = 0; i < 100; i++)
		Q.add(Record(i, 0.0));
	Q.remove(Q.begin());
	EXPECT_EQ(99, Q.size());
}

TEST(CollectionStructuralTest, IterationTest)
{
	Collection<Record> Q;
	for (int i = 0; i < 7; i++)
		Q.add(Record(i, 0.0));
	int k = 0;
	for (Collection<Record>::iterator it = Q.begin(); it != Q.end(); it++) {
		EXPECT_EQ(k, it->a);
		k++;
	}
	EXPECT_EQ(7, k);
}

TEST(CollectionStructuralTest, BackwardsIterationTest)
{
	Collection<Record> Q;
	for (int i = 0; i < 7; i++)
		Q.add(Record(i, 0.0));
	int k = 7;
	Collection<Record>::iterator it = Q.end();
	do {
		it--;
		k--;
		EXPECT_EQ(k, it->a);
	} while (it != Q.begin());
	EXPECT_EQ(0, k);
}

TEST(CollectionStructuralTest, IndexSimpleTest1)
{
	Collection<Record> Q;
	CollectionIndex<Record, int, RecordDispatcher> index;
	Q.add_index(&index);
	for (int i = 0; i < 7; i++)
		Q.add(Record(i, 0.0));
}

TEST(CollectionStructuralTest, IndexRemoveTest1)
{
	Collection<Record> Q;
	CollectionIndex<Record, int, RecordDispatcher> index;
	Q.add_index(&index);
	for (int i = 0; i < 7; i++)
		Q.add(Record(i, 0.0));
	Q.remove(Q.begin());
	EXPECT_EQ(6, Q.size());
}


TEST(CollectionStructuralTest, IndexSearchingTest1)
{
	Collection<Record> Q;
	CollectionIndex<Record, int, RecordDispatcher> index;
	Q.add_index(&index);
	for (int i = 0; i < 7; i++)
		Q.add(Record(i, (double)i));
	
	Collection<Record>::iterator res = index.search(5);
	EXPECT_EQ(5.0, res->b);
	EXPECT_EQ(5, res->a);
}

TEST(CollectionStructuralTest, IndexSearchingTest2)
{
	Collection<Record> Q;
	CollectionIndex<Record, int, RecordDispatcher> index;
	Q.add_index(&index);
	for (int i = 0; i < 7; i++)
		Q.add(Record(i, (double)i));
	Q.add(Record(1337, 5.0));
	Q.add(Record(13, 0.0));
	Q.add(Record(10, 6.0));

	Collection<Record>::iterator res = index.search(1337);
	EXPECT_EQ(5.0, res->b);
	EXPECT_EQ(1337, res->a);
}

TEST(CollectionStructuralTest, IndexSearchingTest3)
{
	Collection<Record> Q;
	for (int i = 0; i < 100; i++)
		Q.add(Record(i, (double)i));

	CollectionIndex<Record, int, RecordDispatcher> index2;
	Q.add_index(&index2);

	Collection<Record>::iterator res = index2.search(7);
	EXPECT_EQ(7.0, res->b);
	EXPECT_EQ(7, res->a);
}

TEST(CollectionStructuralTest, IndexSearchingRemoveTest)
{
	Collection<Record> Q;
	for (int i = 0; i < 3; i++)
		Q.add(Record(i, (double)i));

	CollectionIndex<Record, int, RecordDispatcher> index2;
	Q.add_index(&index2);

	Q.remove(Q.begin());

	Collection<Record>::iterator res = index2.search(2);
	EXPECT_EQ(2.0, res->b);
	EXPECT_EQ(2, res->a);
}

TEST(CollectionStructuralTest, DoubleSearchingRemoveTest)
{
	Collection<Record> Q;
	for (int i = 0; i < 3; i++)
		Q.add(Record(i, (double)i));

	CollectionIndex<Record, double, RecordDoubleDispatcher> index2;
	Q.add_index(&index2);

	Q.remove(Q.begin());

	Collection<Record>::iterator res = index2.search(2.0);
	EXPECT_EQ(2.0, res->b);
	EXPECT_EQ(2, res->a);
}

TEST(CollectionStructuralTest, MultiSearchingTest)
{
	Collection<Record> Q;
	for (int i = 30; i > 0; i--)
		Q.add(Record(i, (double)i));

	CollectionIndex<Record, double, RecordDoubleDispatcher> index1;
	Q.add_index(&index1);

	CollectionIndex<Record, int, RecordDispatcher> index2;
	Q.add_index(&index2);

	Collection<Record>::iterator res = index1.search(2.0);
	EXPECT_EQ(2.0, res->b);
	EXPECT_EQ(2, res->a);

	res = index2.search(15);
	EXPECT_EQ(15.0, res->b);
	EXPECT_EQ(15, res->a);
}

TEST(CollectionStructuralTest, IteratorComparisionTest)
{
	Collection<Record> Q;
	for (int i = 30; i > 0; i--)
		Q.add(Record(i, (double)i));

	CollectionIndex<Record, double, RecordDoubleDispatcher> index1;
	Q.add_index(&index1);

	Collection<Record>::iterator res = index1.search(30.0);
	EXPECT_EQ(30.0, res->b);
	EXPECT_EQ(30, res->a);
	EXPECT_EQ(true, res == Q.begin());
	EXPECT_EQ(false, res != Q.begin());
	EXPECT_EQ(true, res != Q.end());
	EXPECT_EQ(false, res == Q.end());
}