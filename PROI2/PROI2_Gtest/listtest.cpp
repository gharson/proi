#include "gtest/gtest.h"
#include "List.h"

TEST(ListBasicTest, IntCtrTest)
{
	List<int> Q;
}

TEST(ListBasicTest, IntPushTest)
{
	List<int> Q;
	for (int i = 0; i < 10; i++) 
		Q.push_back(i);
}

TEST(ListBasicTest, IntEraseTest)
{
	List<int> Q;
	for (int i = 0; i < 10; i++)
		Q.push_back(i);
	Q.erase(Q.begin());
	EXPECT_EQ(9, Q.size());
}

TEST(ListBasicTest, IntIteratorsTest)
{
	List<int> Q;
	Q.push_back(1337);
	Q.size();
	EXPECT_EQ(1, Q.size());
	EXPECT_EQ(1337, *Q.begin());
}

TEST(ListBasicTest, IntEmptyTest)
{
	List<int> Q;
	for (int i = 0; i < 10; i++) Q.push_back(i);
	
	int k = 0;
	EXPECT_EQ(false, Q.empty());
	while (!Q.empty()) {
		EXPECT_EQ(k, *Q.begin());
		Q.pop_front();
		k++;
	}
}

TEST(ListBasicTest, IntIterationTest)
{
	List<int> Q;
	for (int i = 0; i < 10; i++) Q.push_back(i);

	int k = 0;
	for (List<int>::iterator it = Q.begin(); it != Q.end(); it++) {
		EXPECT_EQ(k, *it);
		k++;
	}
}

TEST(ListBasicTest, IntSizeTest) {
	List<int> Q;
	for (int i = 0; i < 10; i++) Q.push_back(i);
	EXPECT_EQ(10, Q.size());
}