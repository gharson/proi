#include "Osoba.h"

int main() {
	try {
		Osoba man1, man2, man3;
		man1.name = L"Jan";
		man1.surname = L"Kowalski";
		man1.age = 42;
		man1.sex = "MEZCZYZNA";
		man1.addHobby("Narty");
		man1.addHobby("Lyzwy");

		man1.Save("man1.person");

		man2.Load("man1.person");
		//man2.addHobby("Plywanie");

		man3.name = L"Severus";
		man3.surname = L"Snape";
		man3.age = 64;
		man3.sex = "MEZCZYZNA";
		man3.addHobby("Narty");
		man3.addHobby("Ksiazki");

		DisplayableCollection people;
		people.addElement(L"man1", &man1);
		people.addElement(L"man2", &man2);
		people.addElement(L"man3", &man3);

		people.Display();
	}
	catch (exception e) {
		cout << e.what() << endl;
	}
	getchar();
}