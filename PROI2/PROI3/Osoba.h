#pragma once

#include "ASNObject.h"
#include "ASNUTF8String.h"
#include "ASNInteger.h"
#include "ASNEnumerated.h"
#include "ASNSequence.h"
#include "IStorable.h"
#include "IDisplayable.h"
#include "DisplayableCollection.h"

#include <fstream>

class Osoba :
	public ASNObject, public IStorable, public IDisplayable
{
private:
public:
	ASNEnumType sexType;
	ASNEnumType hobbyType;
	ASNUTF8String name;
	ASNUTF8String surname;
	ASNInteger age;
	ASNEnumerated sex;
	ASNSequence hobby;

	DisplayableCollection displayBuffer;

	void addHobby(string);

	Osoba();
	~Osoba();

	void Display();
	void Save(string filename);
	void Load(string filename);

	ostream& writeToStream(ostream&);
	istream& readValueFromStream(istream&);

	void resetBuffer();
};

