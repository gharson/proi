#include "Osoba.h"

Osoba::Osoba() : sexType(72), hobbyType(73), sex(&sexType), displayBuffer("Osoba")
{
	identifier = 200;
	length = 5;
	sexType.addKey("MEZCZYZNA", 0);
	sexType.addKey("KOBIETA", 1);

	hobbyType.addKey("Narty", 0);
	hobbyType.addKey("Lyzwy", 1);
	hobbyType.addKey("Ksiazki", 2);
	hobbyType.addKey("Plywanie", 3);
}

Osoba::~Osoba()
{
}

void Osoba::addHobby(string s) {
	ASNEnumerated *h = new ASNEnumerated(&hobbyType);
	*h = s;
	hobby.addField(h);
}

void Osoba::Display() {
	resetBuffer();
	displayBuffer.Display();
}

/**
@brief Zapisuje do pliku
@details Implementuje zapisywanie do pliku o podanej nazwie.
*/
void Osoba::Save(string filename) {
	ofstream out;
	out.open(filename);
	writeToStream(out);
	out.close();
}

/**
@brief Wczytuje z pliku
@details Implementuje wczytywanie z pliku o podanej nazwie.
*/
void Osoba::Load(string filename) {
	ifstream in;
	in.open(filename);
	char c;
	in.get(c);
	if (c != identifier) throw exception("wrong identifier");
	readValueFromStream(in);
	in.close();
}

/**
@brief Zapisuje serializowane dane do strumienia
@details Implementuje podstawową funkcjonalność serializacji obiektów ASN.1
*/
ostream& Osoba::writeToStream(ostream& os) {
	os.put(identifier);
	os.put(length);
	name.writeToStream(os);
	surname.writeToStream(os);
	age.writeToStream(os);
	sex.writeToStream(os);
	hobby.writeToStream(os);

	return os;
}

/**
@brief Wczytuje dane ze strumienia
@details Implementuje podstawową funkcjonalność serializacji obiektów ASN.1
*/
istream& Osoba::readValueFromStream(istream& is) {
	char c;
	is.get(c);
	is.get(c);
	if (c != ASNTypesIdentifier::UTF8STRING) throw exception("wrong record type (name)");
	name.readValueFromStream(is);
	is.get(c);
	if (c != ASNTypesIdentifier::UTF8STRING) throw exception("wrong record type (surname)");
	surname.readValueFromStream(is);
	is.get(c);
	if (c != ASNTypesIdentifier::INTEGER) throw exception("wrong record type (age)");
	age.readValueFromStream(is);
	is.get(c);
	if (c != ASNTypesIdentifier::ENUMERATED) throw exception("wrong record type (sex)");
	sex.readValueFromStream(is);
	is.get(c);
	if (c != ASNTypesIdentifier::SEQUENCE) throw exception("wrong record type (hobby)");
	hobby.readValueFromStream(is);
	for (int i = 0; i < hobby.data.size(); i++)
		((ASNEnumerated*)hobby.data[i])->setType(&hobbyType);

	return is;
}

/**
@brief Tworzy ponownie strukturę do wyświetlenia na ekranie
*/
void Osoba::resetBuffer() {
	displayBuffer.clear();
	displayBuffer.addElement(L"imie", name.getValue());
	displayBuffer.addElement(L"nazwisko", surname.getValue());
	displayBuffer.addElement(L"wiek", to_wstring(age.getValue()));
	displayBuffer.addElement(L"plec", WstrFromStr(sex.getLabel()));
	string hobbyList;
	for (int i = 0; i < hobby.data.size(); i++) {
		if (i) hobbyList += ", ";
		hobbyList += ((ASNEnumerated*)hobby.data[i])->getLabel();
	}
	displayBuffer.addElement(L"hobby", WstrFromStr(hobbyList));
}