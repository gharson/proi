#ifndef COLLECTION_INDEX_H
#define COLLECTION_INDEX_H

#include "Collection.h"
#include "IndexNode.h"

template <class T> class Collection;

/**
@brief klasa bazowa wszystkich indeksów powiązanych z kolekcją
@see Collection
*/
template <class T>
class BaseCollectionIndex {
protected:
	Collection<T>* collection;
public:
	friend class Collection<T>;
	virtual void add(typename Collection<T>::iterator) = 0;
	virtual void remove(typename Collection<T>::iterator) = 0;
};

/**
@brief Klasa specyfikująca konkretne indeksy
@details Organizuje elementy w strukturę drzewa binarnego
*/
template <class T, class Attribute, class AttributeDispatcher>
class CollectionIndex : public BaseCollectionIndex<T> {
private:
	AttributeDispatcher attribute;
	int compare(T& a, T& b) {
		return attribute(a) > attribute(b);
	}
	typedef IndexNode<typename Collection<T>::iterator> Node;
	Node* root;
public:
	CollectionIndex();
	~CollectionIndex();
	void add(typename Collection<T>::iterator);
	typename Collection<T>::iterator search(const Attribute&);
	void remove(typename Collection<T>::iterator);
};

/**
@brief Konstruktor klasy indeksu
*/
template <class T, class Attribute, class AttributeDispatcher>
CollectionIndex<T, Attribute, AttributeDispatcher>::CollectionIndex<T, Attribute, AttributeDispatcher>() : root(NULL) {}

/**
@brief Destruktor klasy indeksu
*/
template <class T, class Attribute, class AttributeDispatcher>
CollectionIndex<T, Attribute, AttributeDispatcher>::~CollectionIndex<T, Attribute, AttributeDispatcher>(){
}

/**
@brief Dodaje pozycje do indeksu
@details Wstawia wskazany przez podany iterator element do indeksu w celu szybkiego wyszukiwania.
*/
template <class T, class Attribute, class AttributeDispatcher>
void CollectionIndex<T, Attribute, AttributeDispatcher>::add(typename Collection<T>::iterator it) {
	Node *a = root, *prev=NULL, *insert;
	insert = new Node(it);
	while (a != NULL) {
		prev = a;
		if (attribute(*a->value) <= attribute(*it)) a = a->left;
		else a = a->right;
	}
	insert->parent = prev;
	if (prev == NULL) root = insert;
	else {
		if (attribute(*prev->value) <= attribute(*it)) prev->left = insert;
		else prev->right = insert;
	}
}

/**
@brief Wyszukuje elementy w indeksie
@details Korzysta ze struktury drzewa binarnego w celu szybkiego wyszukiwania elementu o wskazanym atrybucie - 
atrybut wybierany jest przez klasę AttributeDispatcher.
@return iterator na znaleziony element lub pusty iterator w przypadku nieznalezienia
*/
template <class T, class Attribute, class AttributeDispatcher>
typename Collection<T>::iterator CollectionIndex<T, Attribute, AttributeDispatcher>::search(const Attribute& pattern) {
	Node *a = root;
	while (a != NULL) {
		if (attribute(*(a->value)) < pattern) a = a->left;
		else if (attribute(*(a->value)) > pattern) a = a->right;
		else return a->value;
	}
	return Collection<T>::nullIterator();
}

/**
@brief Usuwa elementy z indeksu
@details Usuwa element wskazywany przez iterator.
*/
template <class T, class Attribute, class AttributeDispatcher>
void CollectionIndex<T, Attribute, AttributeDispatcher>::remove(typename Collection<T>::iterator it) {
	Node *a = root, *w=NULL;
	while (a != NULL) {
		if (attribute(*a->value) < attribute(*it)) a = a->left;
		else if (attribute(*a->value) > attribute(*it)) a = a->right;
		else {
			w = a;
			break;
		}
	}
	if (w != NULL) {
		if (w->left == NULL || w->right == NULL) {
			Node* son = (w->left == NULL ? w->right : w->left);
			if (son) son->parent = w->parent;
			if (w->parent) {
				if (w->parent->left == w)
					w->parent->left = son;
				if (w->parent->right == w)
					w->parent->right = son;
			}
			else root = son;
		}
		else {
			Node* x = w;
			while (x->right != NULL) x = x->right;
			while (x->left != NULL) x = x->left;
			x->left = w->left;
			if (w->parent) {
				if (w->parent->left == w)
					w->parent->left = w->right;
				if (w->parent->right == w)
					w->parent->right = w->right;
			}
			else root = w->right;
			if(w->left) w->left->parent = x;
		}
		delete w;
	}
}

#endif