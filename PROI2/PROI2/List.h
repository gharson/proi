#ifndef LIST_H
#define LIST_H

#include "ListElement.h"
#include "ListIterator.h"

/**
@brief Lista dwukierunkowa
*/
template <class T>
class List
{
public:
	typedef ListIterator<T> iterator;
	List();
	~List();

	iterator push_back(const T& t);
	iterator push_front(const T& t);
	void pop_back();
	void pop_front();
	iterator erase(iterator);
	void clear();

	iterator begin() const;
	iterator end() const;

	bool empty() const;
	unsigned int size() const;

private:
	ListElement<T> * first_element;
	ListElement<T> * last_element;
	ListElement<T> * void_element;
	int count;
};

template<class T>
List<T>::List<T>() : first_element(NULL), last_element(NULL), void_element(new ListElement<T>), count(0) {
	void_element->next = first_element;
	void_element->prev = last_element;
}

/**
@brief Destruktor listy
@details usuwa wszystkie elementy
*/
template<class T>
List<T>::~List<T>() {
	clear();
}

/**
@brief Dodaje wskazany element na końcu listy
@return iterator na dodany element
*/
template<class T>
typename List<T>::iterator List<T>::push_back(const T& t) {
	ListElement<T> * el = new ListElement<T>(t);
	el->next = void_element;
	el->prev = last_element;
	if (last_element != NULL) last_element->next = el;
	else {
		first_element = el;
		first_element->prev = void_element;
	}
	last_element = el;
	void_element->next = first_element;
	void_element->prev = last_element;
	count++;
	return iterator(last_element);
}

/**
@brief Dodaje wskazany element na początku listy
@return iterator na dodany element
*/
template<class T>
typename List<T>::iterator List<T>::push_front(const T& t) {
	ListElement<T> * el = new ListElement<T>(t);
	el->prev = void_element;
	el->next = first_element;
	if(first_element != NULL) first_element->prev = el;
	else last_element = el;
	first_element = el;
	void_element->next = first_element;
	void_element->prev = last_element;
	count++;
	return iterator(*el);
}

/**
@brief Usuwa wskazany przez iterator element listy
@return iterator na kolejny element na liście
*/
template<class T>
typename List<T>::iterator List<T>::erase(ListIterator<T> it) {
	ListElement<T> * el = it.getNode();
	ListElement<T> * res;
	if (el->prev != void_element) el->prev->next = el->next;
	if (el->next != void_element) el->next->prev = el->prev;
	res = el->next;
	if (el == first_element) first_element = first_element->next;
	if (el == last_element) last_element = last_element->prev;
	delete el;
	if (count == 1) first_element = last_element = NULL;
	count--;
	void_element->next = first_element;
	void_element->prev = last_element;
	return iterator(res);
}

/**
@brief Czyści listę
@details Usuwa wszystkie elementy znajdujące się na liście
*/
template<class T>
void List<T>::clear() {
	while (!empty()) erase(begin());
}

/**
@brief usuwa element z końca listy
*/
template<class T>
void List<T>::pop_back() {
	if (last_element != NULL) {
		if (first_element == last_element) {
			delete last_element;
			first_element = NULL;
			last_element = NULL;
		}
		else {
			ListElement<T> * el = last_element->prev;
			el->next = void_element;
			delete last_element;
			last_element = el;
		}
		void_element->next = first_element;
		void_element->prev = last_element;
		count--;
	}
}

/**
@brief usuwa element z początku listy
*/
template<class T>
void List<T>::pop_front() {
	if (first_element != NULL) {
		if (first_element == last_element) {
			delete first_element;
			first_element = NULL;
			last_element = NULL;
		}
		else {
			ListElement<T> * el = first_element->next;
			el->prev = void_element;
			delete first_element;
			first_element = el;
		}
		void_element->next = first_element;
		void_element->prev = last_element;
		count--;
	}
}

/**
@brief Zwraca iterator oznaczający koniec listy
@return iterator iterator wskazujący na element znajdujący się za ostatnim elementem listy
*/
template<class T>
ListIterator<T> List<T>::end() const {
	return List<T>::iterator(void_element);
}

/**
@brief Zwraca iterator na pierwszy element listy
@return iterator na pierwszy element znajdujący się na liście
*/
template<class T>
ListIterator<T> List<T>::begin() const {
	if (count == 0) return void_element;
	return List<T>::iterator(first_element);
}

/**
@brief Sprawdza czy lista jest pusta
@return true, jeśli lista jest pusta, false w przeciwnym wypadku
*/
template<class T>
bool List<T>::empty() const {
	return first_element == NULL;
}

/**
@brief Zwraca długość listy
@return długość listy będąca nieujemną liczbą całkowitą
*/
template<class T>
unsigned int List<T>::size() const {
	return count;
}

#endif