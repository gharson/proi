#ifndef INDEX_NODE_H
#define INDEX_NODE_H

/**
@brief Węzeł zawierający pojedyńczy element indeksu
@details W praktyce pojedyńczy węzeł drzewa BST zawierający wartość typu T.
*/
template <class T>
class IndexNode {
public:
	T value;
	IndexNode<T>* left;
	IndexNode<T>* right;
	IndexNode<T>* parent;

	IndexNode(T& v);
};

/**
@brief Konstruktor węzła indeksu
*/
template <class T>
IndexNode<T>::IndexNode<T>(T& v) : value(v), left(NULL), right(NULL), parent(NULL) {}
#endif