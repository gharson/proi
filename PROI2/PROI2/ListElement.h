#ifndef LIST_ELEMENT_H
#define LIST_ELEMENT_H

/**
@brief Pojedyńczy element listy dwukierunkowej
@details Wskazuje na element następny i poprzedni.
*/
template <class T>
class ListElement{
public:
	T* value;
	ListElement<T> * next;
	ListElement<T> * prev;
	ListElement(const T& t) : value(new T(t)) {}
	ListElement() : value(NULL) {}
	/**
	@return referencja na zawartość elementu
	*/
	T& getValue() const {
		return *value;
	}
};

#endif