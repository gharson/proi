#include "Collection.h"

struct Record {
	int a;
	double b;
	Record(int x, int y) : a(x), b(y) {}
};

class RecordDispatcher {
public:
	int operator()(Record& r) {
		return r.a;
	}
};


int main() {
	Collection<Record> Q;
	CollectionIndex<Record, int, RecordDispatcher> index;
	Q.add_index(&index);
	for (int i = 0; i < 7; i++)
		Q.add(Record(i, (double)i));

	Collection<Record>::iterator res = index.search(5); // (5, 5.0)
}