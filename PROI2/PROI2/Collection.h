#ifndef COLLECTION_H
#define COLLECTION_H

#include "List.h"
#include "CollectionIndex.h"

/**
@brief Kolekcja obiektów dowolnego typu
@details Może być powiązana z dowolną liczbą indeksów, ułatwiających wyszukiwanie elementów.
@see CollectionIndex
*/
template <class T>
class Collection {
private:
	List<T> data;
	List< BaseCollectionIndex<T>* > index_list;
public:
	~Collection();
	typedef typename List<T>::iterator iterator;
	typedef typename List<BaseCollectionIndex<T>*>::iterator index_iterator;
	static iterator nullIterator();
	typename iterator add(const T&);
	void remove(iterator);
	void add_index(BaseCollectionIndex<T>*);
	int size() const;
	iterator begin() const;
	iterator end() const;
};

/**
@brief Destruktor kolekcji
@details Usuwa wszystkie elementy kolekcji
*/
template <class T>
Collection<T>::~Collection<T>() {
}

/**
@return Pusty iterator
*/
template <class T>
typename Collection<T>::iterator Collection<T>::nullIterator() {
	return iterator(NULL);
}

/**
@brief Wstawia elementy do kolekcji
@return iterator na nowo dodany element
*/
template <class T>
typename Collection<T>::iterator Collection<T>::add(const T& t) {
	iterator insert = data.push_back(t);
	for (index_iterator it = index_list.begin(); it != index_list.end(); it++) {
		(*it)->add(insert);
	}
	return insert;
}

/**
@brief Usuwa elementy z kolekcji
Usuwa z kolekcji element wskazywany przez podany iterator
*/
template <class T>
void Collection<T>::remove(typename Collection<T>::iterator a) {
	for (index_iterator it = index_list.begin(); it != index_list.end(); it++) {
		(*it)->remove(a);
	}
	data.erase(a);
}

/**
@brief Zwraca rozmiar kolekcji
@return liczba elementów w kolecji
*/
template <class T>
int Collection<T>::size() const {
	return data.size();
}

/**
@brief Dodaje indeks do kolekcji
*/
template <class T>
void Collection<T>::add_index(BaseCollectionIndex<T>* index) {
	index_list.push_back(index);
	index->collection = this;
	for (iterator it = begin(); it != end(); it++) {
		index->add(it);
	}
}

/**
@brief Zwraca iterator na pierwszy element kolekcji
@return iterator na pierwszy element znajdujący się w kolekcji
*/
template <class T>
typename Collection<T>::iterator Collection<T>::begin() const {
	return data.begin();
}

/**
@brief Zwraca iterator oznaczający koniec kolekcji
@return iterator iterator wskazujący na element znajdujący się za ostatnim elementem kolekcji
*/
template <class T>
typename Collection<T>::iterator Collection<T>::end() const {
	return data.end();
}

#endif

