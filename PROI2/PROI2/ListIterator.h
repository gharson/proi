#ifndef LIST_ITERATOR_H
#define LIST_ITERATOR_H

#include "ListElement.h"

template <class T> class List;
template <class T> class Collection;

/**
@brief Klasa iteratora listy
@details Przechowuje wska�nik na element listy. Na zewn�trz zachowuje si� jak zwyk�y wska�nik
(przeci��one operatory wy�uskania i dost�pu)
*/
template <class T>
class ListIterator{
private:
	ListElement<T> * node;
	ListIterator(ListElement<T>*);
public:
	friend class List<T>;
	friend class Collection<T>;
	ListIterator(const ListIterator<T>&);
	~ListIterator();
	ListIterator<T> operator++();
	ListIterator<T> operator--();
	T& operator*();
	T* operator->();
	bool operator!=(const ListIterator&) const;
	bool operator==(const ListIterator&) const;
	bool empty() const;
	ListElement<T>* getNode();
};

/**
@brief Konstruktor iteratora
@details Tworzy iterator wskazuj�cy na wskazany element
*/
template <class T>
ListIterator<T>::ListIterator<T>(ListElement<T>* el) : node(el) {}

/**
@brief destruktor iteratora
*/
template <class T>
ListIterator<T>::~ListIterator<T>() {
}

/**
@brief Konstruktor kopiuj�cy iteratora
*/
template <class T>
ListIterator<T>::ListIterator<T>(const ListIterator<T>& it) : node(it.node) {}

/**
@brief Prze�adowany operator postinkrementacji
@details przesuwa iterator na nast�pn� pozycj�
*/
template <class T>
ListIterator<T> ListIterator<T>::operator++() {
	ListIterator<T> it = ListIterator<T>(*this);
	if (node->next != NULL) node = node->next;
	return it;
}

/**
@brief Prze�adowany operator postdekrementacji
@details przesuwa iterator na poprzedzaj�c� go pozycj�
*/
template <class T>
ListIterator<T> ListIterator<T>::operator--() {
	ListIterator<T> it = ListIterator<T>(*this);
	if (node->prev != NULL) node = node->prev;
	return it;
}

/**
@brief Prze�adowany operator wy�uskania
*/
template <class T>
T& ListIterator<T>::operator*() {
	return node->getValue();
}

/**
@brief Prze�adowany operator dost�pu
*/
template <class T>
T* ListIterator<T>::operator->() {
	return node->value;
}

/**
@brief Prze�adowany operator nier�wno�ci
*/
template <class T>
bool ListIterator<T>::operator!=(const ListIterator& a) const {
	return node != a.node;
}

/**
@brief Prze�adowany operator r�wno�ci
*/
template <class T>
bool ListIterator<T>::operator==(const ListIterator& a) const {
	return node == a.node;
}

/**
@brief Sprawdza czy iterator jest pusty
@return true, je�li iterator nie wskazuje na �aden element, false w przeciwnym wypadku
*/
template <class T>
bool ListIterator<T>::empty() const{
	return node == NULL;
}

/**
@return element listy
*/
template <class T>
ListElement<T>* ListIterator<T>::getNode() {
	return node;
}
#endif