#include "gtest/gtest.h"
#include "ASNEnumType.h"
#include "ASNEnumerated.h"

#include <string>
#include <sstream>

using namespace std;

TEST(EnumBasicTest, BuildTest)
{
	ASNEnumType type(100);
	type.addKey("ONE", 1);
	type.addKey("TWO", 2);
	ASNEnumerated e(&type);
}

TEST(EnumBasicTest, GetSetTest)
{
	ASNEnumType type(100);
	type.addKey("ONE", 1);
	type.addKey("TWO", 2);
	ASNEnumerated e(&type);
	e.setValueByKey(1);
	EXPECT_EQ(1, e.getValue());
	EXPECT_STREQ("ONE", e.getLabel().c_str());
}

TEST(EnumSerializationTest, InOutCompare)
{
	ASNEnumType type(100);
	type.addKey("ONE", 1);
	type.addKey("TWO", 2);
	ASNEnumerated a(&type);
	a.setValueByKey(1);
	stringstream ss;
	a.writeToStream(ss);
	string s = ss.str();
	char c;
	ss.get(c);
	ASNEnumerated b(&type);
	b.readValueFromStream(ss);
	EXPECT_EQ(1, b.getValue());
	EXPECT_STREQ("ONE", b.getLabel().c_str());
}