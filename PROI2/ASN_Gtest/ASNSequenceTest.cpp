#include "gtest/gtest.h"
#include "ASNInteger.h"
#include "ASNUTF8String.h"
#include "ASNSequence.h"

#include <string>
#include <sstream>

using namespace std;

TEST(SequenceBasicTest, BuildTest)
{
	ASNSequence a;
	ASNInteger b;
	a.addField(&b);
}

TEST(SequenceSerializationTest, InOutCompare)
{
	ASNInteger a(1337);
	ASNSequence s;
	s.addField(&a);
	stringstream ss;
	s.writeToStream(ss);
	char c;
	ss.get(c);
	ASSERT_EQ(ASNTypesIdentifier::SEQUENCE, c);
	ASNSequence b;
	b.readValueFromStream(ss);
	EXPECT_EQ(1337, ((ASNInteger*)b.data[0])->getValue());
}

TEST(SequenceSerializationTest, MoreInOutCompare)
{
	ASNInteger a(1337);
	ASNUTF8String str(L"lorem");
	ASNSequence s;
	s.addField(&a);
	s.addField(&str);
	stringstream ss;
	s.writeToStream(ss);
	char c;
	ss.get(c);
	ASSERT_EQ(ASNTypesIdentifier::SEQUENCE, c);
	ASNSequence b;
	b.readValueFromStream(ss);
	EXPECT_EQ(1337, ((ASNInteger*)b.data[0])->getValue());
	EXPECT_STREQ(L"lorem", ((ASNUTF8String*)b.data[1])->getValue().c_str());
}