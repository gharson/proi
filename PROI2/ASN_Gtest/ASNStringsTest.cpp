#include "gtest/gtest.h"
#include "ASNUTF8String.h"
#include "ASNBitString.h"

#include <string>
#include <sstream>

using namespace std;

TEST(UTF8StringSerializationTest, InOutCompare)
{
	ASNUTF8String a(L"Fizg�y�");
	stringstream ss;
	a.writeToStream(ss);
	string s = ss.str();
	EXPECT_EQ(2+sizeof(wchar_t)*7 , s.length());
	char c;
	ss.get(c);
	ASSERT_EQ(ASNTypesIdentifier::UTF8STRING, c);
	ASNUTF8String b;
	b.readValueFromStream(ss);
	EXPECT_STREQ(L"Fizg�y�", b.getValue().c_str());
}

TEST(BitStringSerializationTest, InOutCompare)
{
	ASNBitString a("LoremIpsum");
	stringstream ss;
	a.writeToStream(ss);
	string s = ss.str();
	EXPECT_EQ(12, s.length());
	char c;
	ss.get(c);
	ASSERT_EQ(ASNTypesIdentifier::BITSTRING, c);
	ASNBitString b;
	b.readValueFromStream(ss);
	EXPECT_STREQ("LoremIpsum", b.getValue().c_str());
}