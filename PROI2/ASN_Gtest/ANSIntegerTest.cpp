#include "gtest/gtest.h"
#include "ASNInteger.h"

#include <string>
#include <sstream>

using namespace std;

TEST(IntegerBasicTest, BuildTest)
{
	ASNInteger a;
}

TEST(IntegerSerializationTest, InOutCompare)
{
	ASNInteger a(1337);
	stringstream ss;
	a.writeToStream(ss);
	string s = ss.str();
	EXPECT_EQ(6, s.length());
	char c;
	ss.get(c);
	ASSERT_EQ(ASNTypesIdentifier::INTEGER, c);
	ASNInteger b;
	b.readValueFromStream(ss);
	EXPECT_EQ(1337, b.getValue());
}